This is a minimal example that demonstrates running a VNC server in the container to view the GUI of evince.

1) Build this Docker image:
```
$ docker build -t minimal_example ./
```

2) Run docker with exposing port 5900:
```
$ docker run --publish 5900:5900 -it minimal_example
```

3) In docker run teh script that launches x11vnc and starts evince and leave this running in the terminal:
```
# ./root/entrypoint.sh
```

4) Launch the VNC Viewer and connect to `0:0:0:0:5900` 


Sources:
[View a Docker container’s display using VNC Viewer](https://qxf2.com/blog/view-docker-container-display-using-vnc-viewer/)
[How to Run GUI Applications in a Docker Container](https://www.cloudsavvyit.com/10520/how-to-run-gui-applications-in-a-docker-container/)
[VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/macos/)
