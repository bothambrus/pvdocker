FROM ubuntu:20.04

#
# Install packages
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      software-properties-common \
 && apt-get update \
 && apt-get install -y \
      python-is-python3 \
 && apt-get install -y \
      python3 \
      python3-pip \
      python3-venv \
      curl \
      wget \
      tar \
      git \
      vim \
      build-essential \
 && rm -rf /var/lib/apt/lists/*

#
# Print python version
#
RUN set -x \
 && echo "PYTHON VERSION: " \
 && python --version

#
# Install dependencies of paraview 
#
ENV DEBIAN_FRONTEND=noninteractive
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      cmake \
      libgl1-mesa-dev \
      libxt-dev \
      qt5-default \
      libqt5x11extras5-dev \
      libqt5help5 \
      qttools5-dev \
      qtxmlpatterns5-dev-tools \
      libqt5svg5-dev \
      python3-dev \
      python3-numpy \
      libopenmpi-dev \
      libtbb-dev \
      ninja-build \
 && rm -rf /var/lib/apt/lists/*


#
# Install QT5 dependencies 
# following https://github.com/microsoft/vcpkg/issues/15150
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      libx11-dev \
      libx11-xcb-dev \
      libxext-dev  \
      libxfixes-dev \
      libxi-dev \
      libxrender-dev \
      libxcb1-dev \
      libxcb-glx0-dev \
      libxcb-keysyms1-dev \
      libxcb-image0-dev \
      libxcb-shm0-dev \
      libxcb-icccm4-dev \
      libxcb-sync-dev \
      libxcb-xfixes0-dev \
      libxcb-shape0-dev \
      libxcb-randr0-dev \
      libxcb-render-util0-dev \
      libxcb-xinerama0-dev \
      libxkbcommon-dev \
      libxkbcommon-x11-dev \
      libxcb-util-dev \
 && rm -rf /var/lib/apt/lists/*



#
# Clone Paraview  
#
RUN set -x \
 && cd \
 && pwd \
 && git clone --recursive https://gitlab.kitware.com/paraview/paraview.git 2> /dev/null || (cd paraview ; git pull ; cd ..) \
 && cd paraview \
 && ls 

#
# Paraview check out specific version 
#
RUN set -x \
 && cd \
 && pwd \
 && cd paraview \
 && git fetch origin \
 && git checkout v5.10.0 \
 && git submodule update \
 && ls 


#
# Paraview build
#
RUN set -x \
 && cd \
 && pwd \
 && rm -rf build \
 && mkdir -p build \
 && cd build \
 && cmake ../paraview \
    -DPARAVIEW_USE_PYTHON=ON \
    -DPARAVIEW_USE_MPI=ON \
    -DPARAVIEW_USE_VTKM=OFF \
    -DVTK_SMP_IMPLEMENTATION_TYPE=TBB \
    -DCMAKE_BUILD_TYPE=Release \
 && touch buildError.log \
 && make -j 4 install  2> buildError.log || (cat buildError.log) \
 && ls \
 && ls bin/ \
 && ls /usr/local/bin/ 



#
# Set up for VNC connection
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      x11vnc \
      xvfb \
      net-tools \
 && rm -rf /var/lib/apt/lists/*


#
# Install sshfs
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      sshfs \
 && rm -rf /var/lib/apt/lists/*



#
# Expose porti for connecting VNC
#
EXPOSE 5900

# 
# Set display
#
ENV DISPLAY :0
ENV SCREEN_DIMS 1368x768

#
# Create script for running server and paraview
#
RUN  rm -rf /usr/local/bin/vnc_and_paraview.sh \
  && touch /usr/local/bin/vnc_and_paraview.sh \
  && chmod 755 /usr/local/bin/vnc_and_paraview.sh \
  && echo "Xvfb :0 -screen 0 \${SCREEN_DIMS}x16 &" >> /usr/local/bin/vnc_and_paraview.sh \
  && echo "x11vnc  -ncache 10 -display :0 -N -forever &" >> /usr/local/bin/vnc_and_paraview.sh \
  && echo "paraview &" >> /usr/local/bin/vnc_and_paraview.sh 


#
# Create script for setting remote login details 
#
RUN  rm -rf /usr/local/bin/set_remote_details.sh \
  && touch /usr/local/bin/set_remote_details.sh \
  && chmod 755 /usr/local/bin/set_remote_details.sh \
  && echo "#!/bin/bash" >> /usr/local/bin/set_remote_details.sh \
  && echo "set_remote_details()" >> /usr/local/bin/set_remote_details.sh \
  && echo "{" >> /usr/local/bin/set_remote_details.sh \
  && echo "    export REMOTE_USER=\$1" >> /usr/local/bin/set_remote_details.sh \
  && echo "    export REMOTE_MACHINE=\$2" >> /usr/local/bin/set_remote_details.sh \
  && echo "    export REMOTE_PORT=\$3" >> /usr/local/bin/set_remote_details.sh \
  && echo "    export REMOTE_PATH=\$4" >> /usr/local/bin/set_remote_details.sh \
  && echo "}" >> /usr/local/bin/set_remote_details.sh \
  && echo "" >> /usr/local/bin/set_remote_details.sh \
  && echo "set_remote_details \$@" >> /usr/local/bin/set_remote_details.sh \
  && echo "echo \"Variables exported: \nREMOTE_USER=\${REMOTE_USER} \nREMOTE_MACHINE=\${REMOTE_MACHINE} \nREMOTE_PORT=\${REMOTE_PORT} \nREMOTE_PATH=\${REMOTE_PATH} \"" >> /usr/local/bin/set_remote_details.sh \
  && echo "" >> /usr/local/bin/set_remote_details.sh 



#
# Create script for mounting the remote file system
#
RUN  rm -rf /usr/local/bin/mount_remote.sh \
  && touch /usr/local/bin/mount_remote.sh \
  && chmod 755 /usr/local/bin/mount_remote.sh \
  && echo "#!/bin/bash" >> /usr/local/bin/mount_remote.sh \
  && echo "mount_remote()" >> /usr/local/bin/mount_remote.sh \
  && echo "{" >> /usr/local/bin/mount_remote.sh \
  && echo "    mkdir -p /root/remote" >> /usr/local/bin/mount_remote.sh \
  && echo "    echo \"Mounting: \${REMOTE_PATH} of \${REMOTE_MACHINE} to /root/remote/\"" >> /usr/local/bin/mount_remote.sh \
  && echo "    sshfs \${REMOTE_USER}@\${REMOTE_MACHINE}:\${REMOTE_PATH} /root/remote" >> /usr/local/bin/mount_remote.sh \
  && echo "}" >> /usr/local/bin/mount_remote.sh \
  && echo "" >> /usr/local/bin/mount_remote.sh \
  && echo "mount_remote \$@" >> /usr/local/bin/mount_remote.sh \
  && echo "" >> /usr/local/bin/mount_remote.sh 


#
# Create script for logging into a machine with port forwarding
#
RUN  rm -rf /usr/local/bin/ssh_port_forward.sh \
  && touch /usr/local/bin/ssh_port_forward.sh \
  && chmod 755 /usr/local/bin/ssh_port_forward.sh \
  && echo "#!/bin/bash" >> /usr/local/bin/ssh_port_forward.sh \
  && echo "ssh_port_forward()" >> /usr/local/bin/ssh_port_forward.sh \
  && echo "{" >> /usr/local/bin/ssh_port_forward.sh \
  && echo "    echo \"Connecting to: \${REMOTE_MACHINE} with user \${REMOTE_USER} forwarding the port \${REMOTE_PORT}\"" >> /usr/local/bin/ssh_port_forward.sh \
  && echo "    ssh -4 -L \${REMOTE_PORT}:localhost:\${REMOTE_PORT} \${REMOTE_USER}@\${REMOTE_MACHINE}" >> /usr/local/bin/ssh_port_forward.sh \
  && echo "}" >> /usr/local/bin/ssh_port_forward.sh \
  && echo "" >> /usr/local/bin/ssh_port_forward.sh \
  && echo "ssh_port_forward \$@" >> /usr/local/bin/ssh_port_forward.sh \
  && echo "" >> /usr/local/bin/ssh_port_forward.sh 




#
# More cleaning
#
RUN set -x \
 && cd \
 && pwd \
 && rm -rf paraview
