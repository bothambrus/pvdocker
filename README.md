# PVDocker

This repository uses the gitlab pipeline to build a Docker image building [ParaView](https://github.com/Kitware/ParaView/blob/master/Documentation/dev/build.md).

The main application of this docker image is to [launch a paraview server on a remote machine and connect to it from within the docker container](https://gitlab.com/bothambrus/pvdocker#connecting-paraview-in-docker-container-to-a-remote-server).



&nbsp;

## Getting docker on your local machine 

On Ubuntu docker can be installed [from the command line](https://docs.docker.com/engine/install/ubuntu/) as usual.

If you have installed with `snap`, and you want to modify the default `Docker Root Dir` where all the docker data is stored, you can do: 
```
echo '/media/aboth/DockerPartition /var/snap/docker/common/var-lib-docker none bind' >> /etc/fstab
```
based on this [solution](https://stackoverflow.com/questions/64088801/change-docker-snap-data-root-folder).


On Mac the best is to install the [Docker Desktop](https://docs.docker.com/desktop/mac/install/). 
(The docker installed by `brew` has knowns issues.) 
You may need to select a previous version depending on the age of your OS. (E.g.: on macOS Sierra (10.12.6) the last working docker version is: [18.06.1-ce](https://docs.docker.com/desktop/previous-versions/archive-mac/) ) 


&nbsp;

## Getting VNC Viewer on your local machine


Install [VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/) by following the instructions.
You only need  to install the "Viewer" on your local machine, "Server" will be run in the Docker container.


&nbsp;

## Get the local docker image 

1) Getting image:
```
$ sudo docker pull registry.gitlab.com/bothambrus/pvdocker:latest
```

2) Try the container:
```
$ sudo docker run -it registry.gitlab.com/bothambrus/pvdocker
# ls
bin  boot  dev  etc  home  lib  lib32  lib64  libx32  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
# pvserver --version
paraview version 5.10.0
# exit
$
```
Option `-i` starts an interactive(i) session, i.e.: docker will read the standard input, while option `-t` puts the standard output and error on the terminal(t). 


3) Displaying locally available docker images:
```
$ sudo docker image ls
REPOSITORY                                     TAG               IMAGE ID       CREATED          SIZE
registry.gitlab.com/bothambrus/pvdocker        latest            9c37f7b6a410   23 minutes ago   4.58GB
...
```

4) Displaying locally available docker containers:
```
$ sudo docker container ls -a
CONTAINER ID        IMAGE                                     COMMAND             CREATED              STATUS                          PORTS               NAMES
01f64d55c731        registry.gitlab.com/bothambrus/pvdocker   "bash"              About a minute ago   Exited (0) About a minute ago                       vigorous_feynman
...
```

You might want to delete the old docker containers from time to time, if their `STATUS` is "`Exited`" to save storage space on your local machine.
This is done by specifying the "`CONTAINER ID`" of the containers to remove in the `docker rm` command:
```
$ sudo docker rm 01f64d55c731
```



&nbsp;

## Creating singularity image from docker image

Based on this [article](https://www.nas.nasa.gov/hecc/support/kb/converting-docker-images-to-singularity-for-use-on-pleiades_643.html):
```
$ singularity pull pv_singularity.sif docker://registry.gitlab.com/bothambrus/pvdocker:latest
```

Alternatively you can try [`docker2singularity`](https://github.com/singularityhub/docker2singularity) if `singularity` itself is not available.


This is advantegous for running the pvserver on for example MareNostrum, where the only container option is singularity. 
Note, that this is already done, and you are free to use the singularity images:
```
/gpfs/scratch/bsc21/bsc21304/pvser_5_10/pv_singularity.sif
/gpfs/scratch/bsc21/bsc21304/pvser_5_10/pv_osmesa_singularity.sif
```
The first is a singularity image created from this repository, and the second is created from [PVDockerOSMesa](https://gitlab.com/bothambrus/pvdockerosmesa) that is compiled with Off-screen Mesa, and hence it does not need access to x11. 



&nbsp;

## Running local paraview in docker

To run paraview client inside the container on a local machine, the docker container has to be launched with linking the correct ports.
Note, that this is not very useful on its own, as you can just install paraview on your local machine directly. 
The steps here are described, for better debugging and practice. 

1) Run the container in a terminal and add your home as a volume so some files can be opened:
```
$ sudo docker run --publish 5900:5900 --volume ~:/my_home -it registry.gitlab.com/bothambrus/pvdocker
```
Here the `--publish` option exposes a specific port of the container, so the connection can be established between the VNC server running in the container, and the VNC client running on your local machine. 
The `--volume` option mounts the specified directories of your local machine inside the container. 


2) Run the `/usr/local/bin/vnc_and_paraview.sh` bash script inside the container. It will keep running and return the prompt to the user:
```
# vnc_and_paraview.sh
###############################################################
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#@                                                           @#
#@  **  WARNING  **  WARNING  **  WARNING  **  WARNING  **   @#
#@                                                           @#
#@        YOU ARE RUNNING X11VNC WITHOUT A PASSWORD!!        @#
#@                                                           @#
#@  This means anyone with network access to this computer   @#
#@  may be able to view and control your desktop.            @#
#@                                                           @#
#@ >>> If you did not mean to do this Press CTRL-C now!! <<< @#
#@                                                           @#
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
...
20/01/2022 16:55:02

The VNC desktop is:      1a66084153c0:0
PORT=5900
...
```
This bash script starts the VNC server in the docker container, launches ParaView in it. 


3) In a separate teminal of your local machine check the currently running containers:
```
$ docker ps
CONTAINER ID        IMAGE                                     COMMAND             CREATED             STATUS              PORTS                    NAMES
ff41437bd71a        registry.gitlab.com/bothambrus/pvdocker   "bash"              17 seconds ago      Up 16 seconds       0.0.0.0:5900->5900/tcp   angry_ptolemy
```

4) Open a new window of the VNC viewer on your local machine, and connect to this port (`0.0.0.0:5900`) by pasting it into the address bar of VNC viewer.




&nbsp;

## Connecting paraview in docker container to a remote server 

To connect the paraview instance of the docker container to a remote paraview server, one of the easiest solutions is to `ssh` into this server with port-forwarding _from within the docker container_. This way it is unnecessary to expose or publish more ports of the container, so the process is fairly similar to the steps of just [running locally](https://gitlab.com/bothambrus/pvdocker#running-local-paraview-in-docker). 



1) Make a singularity image of the [OSMesa version](https://gitlab.com/bothambrus/pvdockerosmesa) of this docker image as explained [above](https://gitlab.com/bothambrus/pvdocker#creating-singularity-image-from-docker-image) or just use the version already available on MareNostrum: `/gpfs/scratch/bsc21/bsc21304/pvser_5_10/pv_osmesa_singularity.sif`. 

2) If you considered it necessary to make a new singularity image, then upload this image to the remote server:
```
$ scp pv_osmesa_singularity.sif <your_username>@<address_of_remote_machine>:<path_to_singularity_image>  
```

3) Run the container in a terminal and add your home as a volume so some files can be opened. 
You can already assign the environment variables you want for the connection, and skip step 7. 
Note, that the `REMOTE_PORT` variable should be customized, to find a free port of the remote server, that nobody else is using. 
```
sudo docker run --privileged=true  --publish 5900:5900 --env REMOTE_USER=<your_username> --env REMOTE_MACHINE=<address_of_remote_machine> --env REMOTE_PORT=11111 --env REMOTE_PATH=<remote_machine_path_to_mount_in_container> --volume ~:/my_home -it registry.gitlab.com/bothambrus/pvdocker
```

For example: 
```
sudo docker run --privileged=true  --publish 5900:5900 --env REMOTE_USER=bsc21??? --env REMOTE_MACHINE=mn3.bsc.es --env REMOTE_PORT=9876 --env REMOTE_PATH=/gpfs/scratch/bsc21/bsc21???/ --volume ~:/my_home -it registry.gitlab.com/bothambrus/pvdocker
```


Alternatively, without environment variables, the command would simply be:
```
$ sudo docker run --privileged=true  --publish 5900:5900 --volume ~:/my_home -it registry.gitlab.com/bothambrus/pvdocker
```


4) Run the `/usr/local/bin/vnc_and_paraview.sh` bash script inside the container. It will keep running and return the prompt to the user:
```
# vnc_and_paraview.sh
###############################################################
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#@                                                           @#
#@  **  WARNING  **  WARNING  **  WARNING  **  WARNING  **   @#
#@                                                           @#
#@        YOU ARE RUNNING X11VNC WITHOUT A PASSWORD!!        @#
#@                                                           @#
#@  This means anyone with network access to this computer   @#
#@  may be able to view and control your desktop.            @#
#@                                                           @#
#@ >>> If you did not mean to do this Press CTRL-C now!! <<< @#
#@                                                           @#
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
...
20/01/2022 16:55:02

The VNC desktop is:      1a66084153c0:0
PORT=5900
...
```

5) In a separate teminal of your local machine check the currently running containers:
```
$ docker ps
CONTAINER ID        IMAGE                                     COMMAND             CREATED             STATUS              PORTS                    NAMES
ff41437bd71a        registry.gitlab.com/bothambrus/pvdocker   "bash"              17 seconds ago      Up 16 seconds       0.0.0.0:5900->5900/tcp   angry_ptolemy
```

6) Connect the above displayed port (`0.0.0.0:5900`) with VNC Viewer



7) Source the bash script that sets up some environment variables for interacting with the remote server:
Choose a unique port for yourself to avoid problems with other users. Here we use 9876 as a demonstration.
```
# source set_remote_details.sh <your_username> <address_of_remote_machine> 9876 <remote_machine_path_to_mount_in_container>
Variables exported:
REMOTE_USER=<your_username>
REMOTE_MACHINE=<address_of_remote_machine>
REMOTE_PORT=9876
REMOTE_PATH=<remote_machine_path_to_mount_in_container>
```

More specifically, for a MareNostrum user from the CASE department of BSC this should look like:
```
# source set_remote_details.sh bsc21??? mn3.bsc.es 9876 /gpfs/scratch/bsc21/bsc21???/
Variables exported:
REMOTE_USER=bsc21???
REMOTE_MACHINE=mn3.bsc.es
REMOTE_PORT=9876
REMOTE_PATH=/gpfs/scratch/bsc21/bsc21???/
```


8) Mount the requested path of the remote server locally, so you can save the paraview state, and other output of paraview on the remote machine.
Within the docker container you can access your files on this remote path under `/root/remote/`.
Type `yes` to connecting to the "unrecognized machine", as this is the first time the docker container connects to the remote machine.
```
# mount_remote.sh 
Mounting: <remote_machine_path_to_mount_in_container> of <address_of_remote_machine> to /root/remote/ 
The authenticity of host '<address_of_remote_machine> (<IP_of_remote_machine>)' can't be established.
ECDSA key fingerprint is SHA256:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
```

9) From within the terminal of the docker container, connect to the remore server with port forwarding.
This step can be done with a bash script (`/usr/local/bin/ssh_port_forward.sh`) alreay available in the container:
```
# ssh_port_forward.sh 
Connecting to: <address_of_remote_machine> with user <your_username> forwarding the port 9876
```

Optionally this can be done manually as well:
```
# ssh -4 -L 9876:localhost:9876 <your_username>@<address_of_remote_machine>
```
The option `-4` forces the usage of IPv4.  



10) Create a slurm script in the remote server to launch a parallel `pvserver` using the OSMesa singularity image.
The contents of the script should be:
```
#!/bin/bash
#SBATCH -J pvser510
#SBATCH -D .
#SBATCH -o pvser510_%j.out
#SBATCH -e pvser510_%j.err
#SBATCH --ntasks=96
#SBATCH --time=02:00:00
###SBATCH --constraint=highmem
###SBATCH --qos=debug

TN=96
MY_PORT=9876
MY_USER=<your_username>
PV_DIRECTORY=<path_to_singularity_image>

module purge
module load gcc/9.2.0
module load openmpi/4.1.0
module load singularity 

ssh -Y ${MY_USER}@login3 "ssh -Y -4 -N -L ${MY_PORT}:localhost:${MY_PORT} $(hostname)" &
SSHPID=$!
mpirun --oversubscribe -n $TN singularity exec ${PV_DIRECTORY}/pv_osmesa_singularity.sif pvserver --multi-clients --force-offscreen-rendering --server-port=${MY_PORT} > ${PV_DIRECTORY}/pvserver.log 2>&1
kill -9 $SSHPID
```

More specifically, for a MareNostrum user from the CASE department of BSC this slurm script should be:
```
#!/bin/bash
#SBATCH -J pvser510
#SBATCH -D .
#SBATCH -o pvser510_%j.out
#SBATCH -e pvser510_%j.err
#SBATCH --ntasks=96
#SBATCH --time=02:00:00
###SBATCH --constraint=highmem
###SBATCH --qos=debug

TN=96
MY_PORT=9876
MY_USER=bsc21???
PV_DIRECTORY=/gpfs/scratch/bsc21/bsc21304/pvser_5_10/

module purge
module load gcc/9.2.0
module load openmpi/4.1.0
module load singularity 

ssh -Y ${MY_USER}@login3 "ssh -Y -4 -N -L ${MY_PORT}:localhost:${MY_PORT} $(hostname)" &
SSHPID=$!
mpirun --oversubscribe -n $TN singularity exec ${PV_DIRECTORY}/pv_osmesa_singularity.sif pvserver --multi-clients --force-offscreen-rendering --server-port=${MY_PORT} > ${PV_DIRECTORY}/pvserver.log 2>&1
kill -9 $SSHPID
```




11) Launch this slurm script and monitor the log file:
```
> sbatch myslurmscript.sh
> tail -f <path_to_singularity_image>/pvserver.log
Waiting for client...
Connection URL: cs://s01r2b38:11111
Accepting connection(s): s01r2b38:11111
```

12) Once the server is ready to accept connections, go to the VNC Viewer where paraview is opened, and connect to this server:
  - add server 
  - name: machine_name (you can put anything you want)
  - host: localhost
  - port: 11111




&nbsp;

## Aliases and bash functions

Below are some non-essential aliases, that may speed up some of the steps.

### Starting the docker image

You can add a bash function to the `~/.bash_aliases` file of your _local machine_, to facilitate the running of the docker image:
```  
pv_docker() {
   REMOTE_PORT=$1
   sudo docker run --memory 9216M --privileged=true  --publish 5900:5900 --env REMOTE_USER=bsc21??? --env REMOTE_MACHINE=mn3.bsc.es --env REMOTE_PORT=$REMOTE_PORT --env REMOTE_PATH=/gpfs/scratch/bsc21/bsc21??? --volume ~:/my_home -it registry.gitlab.com/bothambrus/pvdocker
}
```
Then you can simply start the docker image by:
```
$ pv_docker 9876
```
The `REMOTE_PORT` is retained as an input, so it can be changed based on availability. 


### Submitting the pvserver job

It is not essential to keep copying the above described slur script to various places, as it changes quite little from case to case. 

You may define a template of it with specific placeholders as: 
```
#!/bin/bash
#SBATCH -J pvser510
#SBATCH -D .
#SBATCH -o pvser510_%j.out
#SBATCH -e pvser510_%j.err
#SBATCH --ntasks=TASKNUMBER
#SBATCH --time=02:00:00
###SBATCH --constraint=highmem
###SBATCH --qos=debug

TN=TASKNUMBER
MY_PORT=PORTNUMBER
MY_USER=bsc21???
PV_DIRECTORY=/gpfs/scratch/bsc21/bsc21304/pvser_5_10/

module purge
module load gcc/9.2.0
module load openmpi/4.1.0
module load singularity 

ssh -Y ${MY_USER}@login3 "ssh -Y -4 -N -L ${MY_PORT}:localhost:${MY_PORT} $(hostname)" &
SSHPID=$!
mpirun --oversubscribe -n $TN singularity exec ${PV_DIRECTORY}/pv_osmesa_singularity.sif pvserver --multi-clients --force-offscreen-rendering --server-port=${MY_PORT} > ${PV_DIRECTORY}/pvserver.log 2>&1
kill -9 $SSHPID
```

You can keep such a file at e.g.: `/gpfs/scratch/bsc21/bsc21???/pvser_5_10/pvserver.sh`


Then a bash function can be defined in your `~/.bash_aliases` file on MareNostrum, that modifies this template, submits the slurm script, and restores the template to its original state:
```
pvser510 () {
    #
    # User inputs
    #
    PVSER_TASKS=$1
    PVSER_Q=$2
    PVSER_PORT=$3
   
    #
    # Confirm user inputs
    #
    echo "Number of processors used: "$PVSER_TASKS
    echo "Queue: "$PVSER_Q
    echo "Port: "$PVSER_PORT

	 PVSCRIPT_PATH=/gpfs/scratch/bsc21/bsc21???/pvser_5_10/

    #
    # SET BATCH FILE
    #
    sed -i 's/TN=TASKNUMBER/TN='"$PVSER_TASKS"'/g' $PVSCRIPT_PATH
    sed -i 's/--ntasks=TASKNUMBER/--ntasks='"$PVSER_TASKS"'/g' $PVSCRIPT_PATH/pvserver.sh
    if [ $PVSER_Q = 'debug'  ]; then
       sed -i 's/\#\#\#SBATCH --qos=debug/\#SBATCH --qos=debug/g' $PVSCRIPT_PATH/pvserver.sh
       echo "Use debug queue!"
    fi
    sed -i 's/MY_PORT=PORTNUMBER/MY_PORT='"$PVSER_PORT"'/g' $PVSCRIPT_PATH/pvserver.sh

    #
    # SUBMIT 
    #
    echo
    echo 'SUBMIT:'
    cat $PVSCRIPT_PATH/pvserver.sh
    sbatch $PVSCRIPT_PATH/pvserver.sh

    #
    # RESET DEFAULT BATCH FILE
    #
    sed -i 's/TN='"$PVSER_TASKS"'/TN=TASKNUMBER/g' $PVSCRIPT_PATH/pvserver.sh
    sed -i 's/--ntasks='"$PVSER_TASKS"'/--ntasks=TASKNUMBER/g' $PVSCRIPT_PATH/pvserver.sh
    if [ $PVSER_Q = 'debug'  ]; then
       sed -i 's/\#SBATCH --qos=debug/\#\#\#SBATCH --qos=debug/g' $PVSCRIPT_PATH/pvserver.sh
    fi
    sed -i 's/MY_PORT='"$PVSER_PORT"'/MY_PORT=PORTNUMBER/g' $PVSCRIPT_PATH/pvserver.sh

    #
    # Follow output of slurm job
    #
    tail -f $PVSCRIPT_PATH/pvserver.log
}
```


Simple pvserver jobs can be aunched as:
```
$ pvser510 96 debug 9876
```





&nbsp;

## Miscellaneous

The gitlab pipeline building this docker image is a copy from [Best practices for building docker images with GitLab CI](https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/).



&nbsp;

To run the ParaView GUI from a docker image without the VNC server you may need to grant rights to docker to access X-Server:
```
xhost +local:docker
```
